
VERSION = 1.8
DIST = flog-$(VERSION)
#DEBUG = -g
CFLAGS = -O2 $(DEBUG) -Wall -pedantic -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE=1
CC = gcc

flog: flog.c
	$(CC) $(CFLAGS) -o $@ $<
	strip $@

clean:
	rm -rf *.o flog $(DIST) $(DIST).tar.gz *~

dist: $(DIST).tar.gz

$(DIST): README Makefile flog.c
	mkdir $(DIST)
	tar c $^ | (cd $(DIST); tar x)

$(DIST).tar.gz: $(DIST)
	tar czf $@ $^
	rm -rf $(DIST)



